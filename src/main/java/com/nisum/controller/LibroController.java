package com.nisum.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nisum.service.LibrosService;
import com.nisum.model.Libro;

@RequestMapping("/libro")
@RestController
public class LibroController {
	
	@Autowired
	private LibrosService libroService;

	@RequestMapping (value = "/crearLibro", method = POST)
	@ResponseBody
	public ResponseEntity<Libro>CrearLibro(@RequestBody Libro libro){
		libroService.guardarLibro(libro);
		return new ResponseEntity<Libro>(libro, HttpStatus.CREATED);
	}
	
	@RequestMapping (value = "/obtenerLibroPorId/{idLibro}", method = GET)
	public ResponseEntity<Libro>obtenerLibro(@PathVariable("idLibro") long idLibro){
		
		return new ResponseEntity<Libro>(libroService.obtenerLibro(idLibro), HttpStatus.OK);
		
	}
	
	@RequestMapping (value = "/obtenerLibroPorTitulo/{tituloLibro}", method = GET)
	public ResponseEntity<Libro>obtenerLibro(@PathVariable("tituloLibro") String tituloLibro){
		
		return new ResponseEntity<Libro>(libroService.obtenerLibro(tituloLibro), HttpStatus.OK);
		
	}
	
	@RequestMapping (value = "/ListarLibros}", method = GET)
	public ResponseEntity<List<Libro>>obtenerLibro(@RequestBody List<Libro> libro){
		
		return new ResponseEntity<List<Libro>>(libro, HttpStatus.OK);
		
	}


}
