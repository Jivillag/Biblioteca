package com.nisum.controller;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import com.nisum.service.LibrosService;
import com.nisum.model.Libro;


@RunWith(MockitoJUnitRunner.class)
public class LibroControllerTest {
	
	@Mock
	private LibrosService libroService;
	
	@InjectMocks
	private LibroController libroController;
	
	
	@Test
	public void crearLibro() {
		RestAssuredMockMvc.standaloneSetup(libroController);
		Libro libro = new Libro();
		libro.setId(1L);
		libro.setTitulo("Orgullo y Prejuicio");
		libro.setAutor("Jane Austen");
		
		Mockito.when(libroService.guardarLibro(Matchers.any(Libro.class))).thenReturn(libro);
		
		given().
			contentType("application/json").
			body(libro).
		when().
			post("/libro/crearLibro").
		then().
			body("titulo", equalTo(libro.getTitulo())).
			statusCode(201);
	}
	
	
	@Test
	public void obtenerLibroPorId(){
		RestAssuredMockMvc.standaloneSetup(libroController);
		Libro libro = new Libro();
		libro.setId(1L);
		
		Mockito.when(libroService.obtenerLibro(anyLong())).thenReturn(libro);
		
		given().
			contentType("application/json").
		when().
			get("/libro/obtenerLibroPorId/{idLibro}",1).
		then().
			body("id", is(1)).
			statusCode(200);
	}
	
	@Test
	public void obtenerLibroPorTitulo(){
		RestAssuredMockMvc.standaloneSetup(libroController);
		Libro libro = new Libro();
		libro.setTitulo("Orgullo y Prejuicio");
		
		Mockito.when(libroService.obtenerLibro(anyString())).thenReturn(libro);
		
		given().
			contentType("application/json").
		when().
			get("/libro/obtenerLibroPorTitulo/{tituloLibro}","Orgullo y Prejuicio").
		then().
			body("titulo", is("Orgullo y Prejuicio")).
			statusCode(200);
	}
	
	@Test
	public void listarLibros() {
		RestAssuredMockMvc.standaloneSetup(libroController);
		
		List<Libro> lista=new ArrayList<Libro>();
		Libro libro=new Libro();
		libro.setId(1L);
		libro.setTitulo("Orgullo y Prejuicio");
		libro.setAutor("Jane Austen");
		lista.add(libro);
		
		Mockito.when(libroService.obtenerLibro(anyLong())).thenReturn((Libro) lista);
		
		given().
			contentType("application/json").
		when().
			get("/libro/listarLibros").
		then().
			body("id", equalTo(libro.getId())).
			statusCode(200);
	}

	


}
